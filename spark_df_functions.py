#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
*** Spark DataFrame Functions ***

NOTES:
	- these functions have not been time tested
	- some of the functions call shell scripts (located in the src directory in this repo)
"""

import subprocess, os
import pyspark.sql.functions as F

#---------------------------------------------------------------#
#### imports that might be necessary for future functions:
# from pyspark.sql import SparkSession
# from pyspark.sql.types import *
#---------------------------------------------------------------#


#TODO: pass shell script path as param
#TODO: function doctrings

#TODO: read file -> sdf fxns
#TODO: fxn to generate schemas?


###############################################################################
#### Functions for writing Spark DataFrames to output files
###############################################################################

def write_sdf_csv_file_no_header(sdf, out_dir, file_name):
	"""csv option 1: write to csv file *WITHOUT* header line

	:param sdf:
	:param out_dir:
	:param file_name:
	:return:
	"""
	# TODO: check if out_dir exists & add error checking --> log file

	tmp_dir = os.path.join(out_dir, 'tmp')

	## write Spark DF to csv file in tmp output directory (unsorted)
	sdf.repartition(1)\
		.write\
		.csv(tmp_dir, header="false", sep=',', mode="overwrite")

	## shell script: 1) sort csv -> txt file, 2) add header, 3) remove tmp directory
	csv_no_header = subprocess.call(
		["src/convert_spark_output_no_header.sh", tmp_dir, out_dir, file_name])

	print("return code = ", csv_no_header)
	if csv_no_header == 0:
		print("Spark DF successfully saved to csv file")
	else:
		print("ERROR occurred when trying to save Spark DF to csv file")

	return csv_no_header


def write_sdf_csv_file_with_header(sdf, out_dir, file_name):
	"""csv option 2: include header before writing to file

	:param sdf:
	:param out_dir:
	:param file_name:
	:return:
	"""
	# TODO: check if out_dir exists & add error checking --> log file

	tmp_dir = os.path.join(out_dir, 'tmp')

	## write Spark DF to csv file in tmp output directory (unsorted)
	sdf.repartition(1)\
		.write\
		.csv(tmp_dir, header="true", sep=',', mode="overwrite")
	
	## shell script: 1) sort csv -> txt file (excluding header line), 2) remove tmp directory	
	csv_with_header = subprocess.call(["src/convert_spark_output_with_header.sh", tmp_dir, out_dir, file_name])

	print("return code = ", csv_with_header)
	if csv_with_header == 0:
		print("Spark DF successfully saved to csv file")
	else:
		print("ERROR occurred when trying to save Spark DF to csv file")
	
	return csv_with_header


def write_sdf_csv_file_add_header(sdf, out_dir, file_name):
	"""write.csv option 3: add header AFTER writing to file
	calls Pyspark write.csv() fxn with header="false"


	:param sdf:
	:param out_dir:
	:param file_name:
	:return:
	"""
	# TODO: check if out_dir exists & add error checking --> log file

	tmp_dir = os.path.join(out_dir, 'tmp')
	header_str = ','.join(c for c in sdf.columns)

	## write Spark DF to csv file in tmp output directory (unsorted)
	sdf.repartition(1)\
		.write\
		.csv(tmp_dir, header="false", sep=',', mode="overwrite")

	## shell script: 1) sort csv -> txt file, 2) add header, 3) remove tmp directory
	csv_add_header = subprocess.call(
		["src/convert_spark_output_add_header.sh", tmp_dir, out_dir, file_name, header_str])

	print("return code = ", csv_add_header)
	if csv_add_header == 0:
		print("Spark DF successfully saved to csv file")
	else:
		print("ERROR occurred when trying to save Spark DF to csv file")

	return csv_add_header




def write_sdf_tsv_file_no_header(sdf, out_dir, file_name):
	"""Write TSV option 1: write to csv file *WITHOUT* header line
		1. call Pyspark write.csv() fxn with header="false" & sep='\t'
		2. sort output file

	:param sdf:
	:param out_dir:
	:param file_name:
	:return:
	"""
	# TODO: check if out_dir exists & add error checking --> log file

	tmp_dir = os.path.join(out_dir, 'tmp')
	# header_str = '\t'.join(c for c in sdf.columns)

	## write Spark DF to csv file in tmp output directory (unsorted)
	sdf.repartition(1)\
		.write\
		.csv(tmp_dir, header="false", sep='\t', mode="overwrite")

	## shell script: 1) sort csv -> txt file, 2) add header, 3) remove tmp directory
	tsv_no_header = subprocess.call(
		["src/convert_spark_output_no_header.sh", tmp_dir, out_dir, file_name])

	print("return code = ", tsv_no_header)
	if tsv_no_header == 0:
		print("Spark DF successfully saved to TSV .txt file")
	else:
		print("ERROR occurred when trying to save Spark DF to TSV file")

	return tsv_no_header


def write_sdf_tsv_file_with_header(sdf, out_dir, file_name):
	"""Write TSV option 2: include header before writing to file
		1. call Pyspark write.csv() fxn with header="true" & sep='\t'
		2. sort output file

	:param sdf:
	:param out_dir:
	:param file_name:
	:return:
	"""
	# TODO: check if out_dir exists & add error checking --> log file

	tmp_dir = os.path.join(out_dir, 'tmp')

	## write Spark DF to csv file in tmp output directory (unsorted)
	sdf.repartition(1)\
		.write\
		.csv(tmp_dir, header="true", sep='\t', mode="overwrite")

	## shell script: 1) sort csv -> txt file (excluding header line), 2) remove tmp directory
	tsv_with_header = subprocess.call(
		["src/convert_spark_output_with_header.sh", tmp_dir, out_dir, file_name])

	print("return code = ", tsv_with_header)
	if tsv_with_header == 0:
		print("Spark DF successfully saved to TSV .txt file")
	else:
		print("ERROR occurred when trying to save Spark DF to TSV file")

	return tsv_with_header


def write_sdf_tsv_file_add_header(sdf, out_dir, file_name):
	"""write TSV option 3: add header AFTER writing to file
		1. call Pyspark write.csv() fxn with header="false" & sep='\t'
		2. sort output file & add header


	:param sdf:
	:param out_dir:
	:param file_name:
	:return:
	"""
	# TODO: check if out_dir exists & add error checking --> log file

	tmp_dir = os.path.join(out_dir, 'tmp')
	header_str = '\t'.join(c for c in sdf.columns)

	## write Spark DF to csv file in tmp output directory (unsorted)
	sdf.repartition(1)\
		.write\
		.csv(tmp_dir, header="false", sep='\t', mode="overwrite")

	## shell script: 1) sort csv -> txt file, 2) add header, 3) remove tmp directory
	tsv_add_header = subprocess.call(
		["src/convert_spark_output_add_header.sh", tmp_dir, out_dir, file_name, header_str])

	print("return code = ", tsv_add_header)
	if tsv_add_header == 0:
		print("Spark DF successfully saved to TSV .txt file")
	else:
		print("ERROR occurred when trying to save Spark DF to TSV file")

	return tsv_add_header




def write_sdf_text_file_no_header(sdf, out_dir, file_name):
	"""write.text: write Spark DF to text file
		1. convert DF to a single column of string type -> write .txt
		2. call subprocess to sort file & add header string

		*NOTE*: canNOT directly include the header using this approach

	:param sdf:
	:param out_dir:
	:param file_name:
	:return:
	"""
	# TODO: check if out_dir exists & add error checking --> log file

	tmp_dir = os.path.join(out_dir, 'tmp')
	header_str = '\t'.join(c for c in sdf.columns)
	header_list = ['\t'] + sdf.columns
	
	## convert Spark DF to text --> write to text file in tmp output directory (unsorted - no header)	
	sdf.select(F.concat_ws(*header_list).alias(header_str))\
		.repartition(1)\
		.write\
		.mode('overwrite')\
		.text(tmp_dir)

	## shell script: 1) sort txt file, 2) add header, 3) remove tmp directory
	txt_no_header = subprocess.call(
		["src/convert_spark_output_no_header.sh", tmp_dir, out_dir, file_name, header_str])

	print("return code = ", txt_no_header)
	if txt_no_header == 0:
		print("Spark DF successfully saved to text file")
	else:
		print("ERROR occurred when trying to save Spark DF to text file")

	return txt_no_header


def write_sdf_text_file_add_header(sdf, out_dir, file_name):
	"""write.text: write Spark DF to text file
		1. convert DF to a single column of string type -> write .txt
		2. call subprocess to sort file & add header string

		*NOTE*: canNOT directly include the header using this approach

	:param sdf:
	:param out_dir:
	:param file_name:
	:return:
	"""
	# TODO: check if out_dir exists & add error checking --> log file

	tmp_dir = os.path.join(out_dir, 'tmp')
	header_str = '\t'.join(c for c in sdf.columns)
	header_list = ['\t'] + sdf.columns

	## convert Spark DF to text --> write to text file in tmp output directory (unsorted - no header)
	sdf.select(F.concat_ws(*header_list).alias(header_str))\
		.repartition(1)\
		.write\
		.mode('overwrite')\
		.text(tmp_dir)

	## shell script: 1) sort txt file, 2) add header, 3) remove tmp directory
	txt_add_header = subprocess.call(
		["src/convert_spark_output_add_header.sh", tmp_dir, out_dir, file_name, header_str])

	print("return code = ", txt_add_header)
	if txt_add_header == 0:
		print("Spark DF successfully saved to text file")
	else:
		print("ERROR occurred when trying to save Spark DF to text file")

	return txt_add_header




def write_sdf_to_bed_file(sdf, out_dir, file_name):
	"""write Spark DF to bed file

	:param sdf:
	:param out_dir:
	:param file_name:
	:return: int indicating success [0] or failure [1]
	"""
	# TODO: check if out_dir exists & add error handling --> log file

	tmp_dir = os.path.join(out_dir, 'tmp')

	## write Spark DF to csv file in tmp output directory (unsorted)
	sdf.repartition(1)\
		.write\
		.csv(tmp_dir, header="false", sep='\t', mode="overwrite")

	## shell script: 1) sort csv -> txt file, 2) add header, 3) remove tmp directory
	bed_file = subprocess.call(
		["src/convert_spark_output_to_bed.sh", tmp_dir, out_dir, file_name])

	print("return code = ", bed_file)
	if bed_file == 0:
		print("Spark DF successfully saved to bed file")
	else:
		print("ERROR occurred when trying to save Spark DF to bed file")

	return bed_file




def write_rdd_text_file_no_header(sdf, out_dir, file_name):
	"""rdd option 1: write rdd to file without header --> add header to text file afterwards

	:param sdf:
	:param out_dir:
	:param file_name:
	:return:
	"""
	# TODO: check if out_dir exists & add error checking --> log file

	tmp_dir = os.path.join(out_dir, 'tmp')

	## convert Spark DF to rdd of strings & repartition
	sdf.rdd\
		.map(lambda line: '\t'.join([str(x) for x in line]))\
		.repartition(1)\
		.saveAsTextFile(tmp_dir)

	## shell script: 1) sort -> txt file, 2) add header, 3) remove tmp directory
	rdd_no_header = subprocess.call(
		["src/convert_spark_output_no_header.sh", tmp_dir, out_dir, file_name])

	print("return code = ", rdd_no_header)
	if rdd_no_header == 0:
		print("Spark RDD successfully saved to text file")
	else:
		print("ERROR occurred when trying to save Spark RDD to text file")

	return rdd_no_header


def write_rdd_text_file_with_header(sdf, out_dir, file_name, spark_context):
	"""rdd option 2: include header in rdd before writing to file

	:param sdf:
	:param out_dir:
	:param file_name:
	:return:
	"""
	# TODO: check if out_dir exists & add error checking --> log file

	tmp_dir = os.path.join(out_dir, 'tmp')
	header_str = '\t'.join(c for c in sdf.columns)
	
	## convert Spark DF to rdd of strings & repartition
	sdf_as_rdd = sdf.rdd\
		.map(lambda line: '\t'.join([str(x) for x in line]))\
		.repartition(1)

	## create rdd from column header list
	header_rdd = spark_context.parallelize([header_str])

	## combine RDDs and saveAsTextFile
	header_rdd.union(sdf_as_rdd)\
		.coalesce(1)\
		.saveAsTextFile(tmp_dir)
	
	## shell script: 1) sort -> txt file (excluding header line), 2) remove tmp directory	
	rdd_with_header = subprocess.call(
		["src/convert_spark_output_with_header.sh", tmp_dir, out_dir, file_name])

	print("return code = ", rdd_with_header)
	if rdd_with_header == 0:
		print("Spark RDD successfully saved to text file")
	else:
		print("ERROR occurred when trying to save Spark RDD to text file")
	
	return rdd_with_header


def write_rdd_text_file_add_header(sdf, out_dir, file_name):
	"""rdd option 3: write rdd to file without header --> add header to text file afterwards

	:param sdf:
	:param out_dir:
	:param file_name:
	:return:
	"""
	# TODO: check if out_dir exists & add error checking --> log file

	tmp_dir = os.path.join(out_dir, 'tmp')
	header_str = '\t'.join(c for c in sdf.columns)

	## convert Spark DF to rdd of strings & repartition
	sdf.rdd\
		.map(lambda line: '\t'.join([str(x) for x in line]))\
		.repartition(1)\
		.saveAsTextFile(tmp_dir)

	## shell script: 1) sort -> txt file, 2) add header, 3) remove tmp directory
	rdd_add_header = subprocess.call(
		["src/convert_spark_output_add_header.sh", tmp_dir, out_dir, file_name, header_str])

	print("return code = ", rdd_add_header)
	if rdd_add_header == 0:
		print("Spark RDD successfully saved to text file")
	else:
		print("ERROR occurred when trying to save Spark RDD to text file")

	return rdd_add_header








###############################################################################
#### Functions for writing & reading sdf <--> parquet file
###############################################################################

def write_input_sv_sdf_to_parquet(sdf, out_dir):
	"""write input SV sdf to parquet file(s)

	:param sdf:
	:param out_dir:
	:return:
	"""
	# TODO: check if out_dir exists & add error checking --> log file

	parquet_name = os.path.join(out_dir, 'input_SV_parquet')
	sdf.write\
		.parquet(parquet_name, mode='overwrite', compression='gzip')



def write_input_sv_sdf_to_parquet_with_name(sdf, out_dir, data_name):
	"""write input SV sdf to parquet file(s) & specify the name of the directory

	:param sdf:
	:param out_dir:
	:param data_name:
	:return:
	"""
	# TODO: check if out_dir exists & add error checking --> log file

	parquet_dir_name = "input_SV_" + data_name + "_parquet"
	print(parquet_dir_name)

	parquet_name = os.path.join(out_dir, parquet_dir_name)
	sdf.write\
		.parquet(parquet_name, mode='overwrite', compression='gzip')




def write_sdf_to_parquet(sdf, out_dir, parquet_dir_name, compression="gzip"):
	"""Generalized function that writes Spark DF to parquet file

	:param sdf:
	:param out_dir:
	:param parquet_dir_name:
	:param compression:
	:return:
	"""
	# TODO: add error checking --> log file

	parquet_name = os.path.join(out_dir, parquet_dir_name)

	sdf.write.parquet(parquet_name, mode='overwrite', compression=compression)

	## test if write was successful
	if os.path.isfile(os.path.join(parquet_name, '_SUCCESS')):
		return_code = 0
		print("return code = ", return_code)
		print("Spark DF successfully saved to parquet")
	else:
		return_code = 1
		print("ERROR occurred when trying to save Spark DF to parquet")

	return return_code







def write_sdf_to_parquet_partition(sdf, out_dir, parquet_dir_name, compression="gzip", partition_cols=None):
	"""Generalized function that writes Spark DF to parquet file

		*NOTE*: using the partitionBy option is EXTREMELY SLOW!

	:param sdf:
	:param out_dir:
	:param parquet_dir_name:
	:param compression:
	:param partition_cols:
	:return:
	"""
	# TODO: check if out_dir exists & add error checking --> log file

	parquet_name = os.path.join(out_dir, parquet_dir_name)

	if partition_cols is None:
		sdf.write\
			.parquet(parquet_name, mode='overwrite', compression=compression)
	else:
		sdf.write\
			.partitionBy(partition_cols)\
			.parquet(parquet_name, mode='overwrite', compression=compression)

	## test if write was successful
	if os.path.isfile(os.path.join(parquet_name, '_SUCCESS')):
		return_code = 0
		print("return code = ", return_code)
		print("Spark DF successfully saved to parquet")
	else:
		return_code = 1
		print("ERROR occurred when trying to save Spark DF to parquet")

	return return_code


def load_sdf_from_parquet_dir(parquet_dir, spark_session):
	"""generalized function that loads Spark DF from parquet files

	:param parquet_dir:
	:param spark_session:
	:return:
	"""
	# TODO: check if parquet dir exists & add error checking --> log file

	parquet_str = parquet_dir + '/part-*'
	sdf = spark_session.read.parquet(parquet_str)

	# sdf = spark_session.read.parquet(parquet_dir)
	return sdf


def load_sdf_from_parquet_partitioned(parquet_dir, spark_session):
	"""generalized function that loads Spark DF from parquet files

	:param parquet_dir:
	:param spark_session:
	:return:
	"""
	# TODO: check if parquet dir exists & add error checking --> log file

	sdf = spark_session.read.parquet(parquet_dir)
	return sdf






###############################################################################
#### Functions for renaming Spark DF columns
###############################################################################

def rename_spark_columns_lowercase(sdf):
	"""convert Spark DF column names to lowercase

	:param sdf:
	:return:
	"""

	columns = dict(zip(sdf.columns, [c.lower() for c in sdf.columns]))
	for old_name, new_name in columns.items():
		sdf = sdf.withColumnRenamed(old_name, new_name)
	return sdf



def rename_spark_columns(sdf, col_dict):
	"""rename Spark DF columns using the supplied dictionary

	:param sdf:
	:param col_dict:
	:return:
	"""

	if isinstance(col_dict, dict):
		for old_name, new_name in col_dict.items():
			sdf = sdf.withColumnRenamed(old_name, new_name)
		return sdf
	else:
		raise ValueError("'col_dict' should be a dict with format:  {'old_name_1':'new_name_1', 'old_name_2':'new_name_2'}")








###############################################################################
#### Functions for 
###############################################################################



