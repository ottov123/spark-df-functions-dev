#!/usr/bin/env bash 
TMPDIR=$1
OUTDIR=$2
FILENAME=$3


## step 1: sort file EXCEPT for header row!
if [ "$(uname)" == "Darwin" ]; then
	gsort -k1,1 -k2,2 -k3,3 -V -s ${TMPDIR}/part* > ${TMPDIR}/${FILENAME}

else
	sort -k1,1 -k2,2 -k3,3 -V -s ${TMPDIR}/part* > ${TMPDIR}/${FILENAME}

fi


## step 2: move new sorted output file & delete tmp spark outout directory
mv ${TMPDIR}/${FILENAME} ${OUTDIR}/${FILENAME}
rm -r ${TMPDIR}


